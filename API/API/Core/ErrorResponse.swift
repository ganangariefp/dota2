//
//  ErrorResponse.swift
//  API
//
//  Created by Ganang Arief Pratama on 05/07/21.
//

import Foundation

/// Error model from api, to simplify high level information
public struct ErrorResponse {
    
    public var status: Int
    public var message: String
    
    public init(status: Int, message: String) {
        self.status = status
        self.message = message
    }
}
