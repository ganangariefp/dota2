//
//  HeroStatsResponse.swift
//  API
//
//  Created by Ganang Arief Pratama on 05/07/21.
//

import Foundation

/// Response Model from GET  herostats api
public struct HeroStatsResponse: Codable {
    public var id: Int?
    public var name: String?
    public var localized_name: String?
    public var primary_attr: String?
    public var attack_type: String?
    public var roles: [String]?
    public var img: String?
    public var icon: String?
    public var base_health: Int?
    public var base_health_regen: Double?
    public var base_mana: Int?
    public var base_mana_regen: Double?
    public var base_armor: Double?
    public var base_mr: Int?
    public var base_attack_min: Int?
    public var base_attack_max: Int?
    public var move_speed: Int?
}
