//
//  HeroServices.swift
//  API
//
//  Created by Ganang Arief Pratama on 05/07/21.
//

import Foundation
import Alamofire

/// Movie services
public struct HeroServices {
    
    /**
     Get Hero Stats
     Path: /herostats
     */
    public static func getHeroStats(
        query: String? = nil,
        page: Int? = nil,
        progress: ((Double) -> ())? = nil,
        onSuccess: (([HeroStatsResponse]) -> ())? = nil,
        onFailure: ((ErrorResponse) -> ())? = nil) -> ApiRequest<HeroStatsResponse> {
        
        var params = Api.shared.createParams()
        params["query"] = query
        if let page = page {
            params["page"] = page
        }
        
        return ApiRequest<HeroStatsResponse>(
            path: "/herostats",
            params: params,
            method: .get,
            progress: progress,
            onSuccess: onSuccess,
            onFailure: onFailure
        )
    }
}
