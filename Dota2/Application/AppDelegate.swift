//
//  AppDelegate.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 05/07/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppDelegateCommandBuilder().actions.forEach({$0.execute()})
        
        return true
    }

}

