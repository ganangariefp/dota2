//
//  AppDelegateCommandBuilder.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit
import Foundation
import API

protocol Command {
    func execute()
}

class AppDelegateCommandBuilder {
    let actions: [Command] = [InitializeAPICommand(),
                              InitializeRootViewControllerCommand(),
    ]
}

class InitializeAPICommand: Command {
    func execute() {
        Api.initInstance(apiBasePath: Constant.apiBasePath)
    }
}

class InitializeRootViewControllerCommand: Command {
    
    func execute() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let navController: UINavigationController = UINavigationController()
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        navController.viewControllers = [HomeViewController()]
        window.rootViewController = navController
        window.makeKeyAndVisible()
        appDelegate.window = window
    }
}


