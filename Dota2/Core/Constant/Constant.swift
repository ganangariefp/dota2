//
//  Constant.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation

struct Constant {
    
    //Mark: - URL base path
    static let apiBasePath: String = "https://api.opendota.com/api"
    static let apiBaseImagePath: String = "https://api.opendota.com"
    static let KEY_HERO_CELL: String = "KEY_HERO_CELL_ID"
    static let KEY_SORT_CELL: String = "KEY_SORT_CELL_ID"
    static let KEY_FILTER_CELL: String = "KEY_HERO_CELL_ID"
    static let KEY_HEADER_CELL: String = "KEY_HEADER_CELL_ID"
}
