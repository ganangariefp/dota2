//
//  Array.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit
import API

extension Array {
    func sortedByKindAndOrder(_ array: [HeroStatsResponse], _ kind: String, _ order: String) -> [HeroStatsResponse] {
        
        let array = array.sorted { (initial, next) -> Bool in
            if kind == "base_health" {
                let condition = order == "min" ? initial.base_health! < next.base_health! : initial.base_health! > next.base_health!
                
                return condition
            }
            
            if kind == "base_mana" {
                let condition = order == "min" ? initial.base_mana! < next.base_mana! : initial.base_mana! > next.base_mana!
                
                return condition
            }
            
            if kind == "base_attack_min" {
                let condition = order == "min" ? initial.base_attack_min! < next.base_attack_min! : initial.base_attack_min! > next.base_attack_min!
                
                return condition
            }
            
            if kind == "move_speed" {
                let condition = order == "min" ? initial.move_speed! < next.move_speed! : initial.move_speed! > next.move_speed!
                
                return condition
            }
            
            return initial.id! > next.id!
        }
        
        return array
    }
    
}

