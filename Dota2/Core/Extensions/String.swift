//
//  UISTtring.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit

extension String {
    func roles(_ roles: [String]) -> String {
        var result = "Roles = "
        
        roles.forEach({
            if $0 == roles.last {
                result += $0 + "."
            } else {
                result += $0 + ", "
            }
        })
        
        return result
    }
}
