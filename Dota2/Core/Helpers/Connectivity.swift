//
//  Connectivity.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation
import Alamofire
import API

/// All about connectivity helper
class Connectivity {
    
    /// Check wether device is connected to internet
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    /// Error model `No internet connection`
    class var error: ErrorResponse {
        return ErrorResponse(status: 502,
                             message: "Internet connection appears to be offline")
    }
}
