//
//  RequestState.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation

enum RequestState {
    case ready
    case loading
    case success
    case error
}
