//
//  CoreDataManager.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 07/07/21.
//

import Foundation
import CoreData
import API

public class CoreDataManager {
    public static let shared = CoreDataManager()
    
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "HeroesModel")
        container.loadPersistentStores { (storeDescription, error) in
            if let error = error {
                fatalError("Loading of store failed \(error)")
            }
        }
        
        return container
    }()
    
    public func addFavoriteHero(_ hero: HeroStatsResponse, onSuccess: @escaping (HeroEntity) -> Void, onError: @escaping (Error) -> Void) {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<HeroEntity>(entityName: "HeroEntity")
        
        do {
            let heroes = try context.fetch(fetchRequest)
            let filter = heroes.filter { $0.id == hero.id! }
            
            if filter.count == 0 {
                do {
                    let heroEntity = HeroEntity(context: context)
                    heroEntity.id = Int64(hero.id!)
                    heroEntity.name = hero.localized_name
                    heroEntity.attack_type = hero.attack_type
                    heroEntity.primary_attr = hero.primary_attr
                    let url = URL(string: Constant.apiBaseImagePath + hero.img!)
                    
                    do {
                        heroEntity.img = try Data(contentsOf: url!)
                    } catch{
                        print("Unable to load data: \(error)")
                    }
                    try context.save()
                    onSuccess(heroEntity)
                } catch let createError {
                    onError(createError)
                }
            }
            
        } catch let fetchError {
            onError(fetchError)
        }
        
    }
    
    func fetchHeroes(onSuccess: @escaping ([HeroEntity]) -> Void, onError: @escaping (Error) -> Void) {
        let context = persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<HeroEntity>(entityName: "HeroEntity")
        
        do {
            let heroes = try context.fetch(fetchRequest)
            onSuccess(heroes)
        } catch let fetchError {
            onError(fetchError)
        }
        
    }
    
    func deleteHero(_ hero: HeroEntity) {
        let context = persistentContainer.viewContext
        
        do {
            context.delete(hero)
            try context.save()
        } catch let deleteError {
            print("Failed to delete: \(deleteError)")
        }
    }
    
    func deleteHeroes() {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<HeroEntity>(entityName: "HeroEntity")
        
        do {
            let heroes = try context.fetch(fetchRequest)
            for hero in heroes {
                context.delete(hero)
            }
            try context.save()
        } catch let deleteError {
            print("Failed to delete: \(deleteError)")
        }
    }
    
    
}
