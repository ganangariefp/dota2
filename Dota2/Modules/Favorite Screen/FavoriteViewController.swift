//
//  FavoriteViewController.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 07/07/21.
//

import UIKit
import API

class FavoriteViewController: UIViewController {
    
    var heroes: [HeroEntity] = []
    
    let favoriteCollectionView: UICollectionView = {
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionview.translatesAutoresizingMaskIntoConstraints = false
        collectionview.backgroundColor = .white
        collectionview.showsVerticalScrollIndicator = false
        
        return collectionview
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.title = "Favorites"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        CoreDataManager.shared.deleteHeroes()
        setupViews()
        registerCollectionView()
        getFavoritesHeroesFromLocal()
        registerNotification()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupViews() {
        view.backgroundColor = .white
        
        view.addSubview(favoriteCollectionView)
        favoriteCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        favoriteCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        favoriteCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        favoriteCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

// MARK: Register Notification
extension FavoriteViewController {
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleReceiveEvent), name: NSNotification.Name(rawValue: "update"), object: nil)
    }
    
    @objc func handleReceiveEvent(_ notification: NSNotification) {
        if notification.object as! String == "update" {
            getFavoritesHeroesFromLocal()
        }
    }
}

// MARK: Setup Services
extension FavoriteViewController {
    func getFavoritesHeroesFromLocal() {
        CoreDataManager.shared.fetchHeroes { (heroes) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.heroes = heroes
                self.favoriteCollectionView.reloadData()
            })
        } onError: { (error) in
            print(error)
        }
    }
}

// MARK: Configure collectionview
extension FavoriteViewController {
    func registerCollectionView() {
        favoriteCollectionView.delegate = self
        favoriteCollectionView.dataSource = self
        favoriteCollectionView.register(HeroCell.self, forCellWithReuseIdentifier: Constant.KEY_HERO_CELL)
    }
}

// MARK: Handle menu collectionview event
extension FavoriteViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return heroes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.KEY_HERO_CELL, for: indexPath) as! HeroCell
        cell.layer.cornerRadius = 8
        cell.backgroundColor = .black
        cell.setHeroFavorite(heroes[indexPath.row])
        cell.favoriteButton.isHidden = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 - 24, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let hero = heroes[indexPath.row]
        handleUnfavorite(hero)
    }
}

extension FavoriteViewController {
    func handleUnfavorite(_ hero: HeroEntity) {
        let alert = UIAlertController(title: "Unfavorite", message: "Do you want to unfavorite?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Unfavorite", style: UIAlertAction.Style.destructive, handler: { action in
            self.unfavoriteHero(hero)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func unfavoriteHero(_ hero: HeroEntity) {
        CoreDataManager.shared.deleteHero(hero)
        getFavoritesHeroesFromLocal()
    }
}
