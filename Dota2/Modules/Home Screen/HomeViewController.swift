//
//  HomeViewController.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 07/07/21.
//

import UIKit
import API

class HomeViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureInterfaceComponent()
        configureTabbar()
    }
    
    func configureInterfaceComponent() {
        view.backgroundColor = .white
    }
    
    func configureTabbar() {
        let mainVC = MainViewController()
        mainVC.tabBarItem = UITabBarItem.init(title: "Home", image: nil, selectedImage: nil)

        let favoriteVC = FavoriteViewController()
        favoriteVC.tabBarItem = UITabBarItem.init(title: "Favorite", image: nil, selectedImage: nil)

        let tabBarList = [mainVC, favoriteVC]
        viewControllers = tabBarList
    }
}
