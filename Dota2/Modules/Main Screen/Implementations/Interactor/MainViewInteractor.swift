//
//  MainViewInteractor.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit
import API
import Alamofire

class MainViewInteractor: MainViewInteractorProtocol {
    weak var presenter: MainViewPresenterProtocol?
    
    var response: [HeroStatsResponse]!
    var request: DataRequest?
    
    private var requestState: RequestState = .ready
    
    func fetchHeroStats(withQuery query: String?) {
        guard requestState != .loading else { return }
        if !Connectivity.isConnectedToInternet {
            presenter?.handleHeroStatsErrorRequest(response: Connectivity.error)
        }
        requestState = .loading
        request = HeroServices.getHeroStats(
            query: query,
            page: nil,
            onSuccess: { [weak self] response in
                self?.response = response
                self?.requestState = .success
                self?.presenter?.handleHeroStatsSuccessRequest(response: response)
            },
            onFailure: { [weak self] error in
                self?.requestState = .error
                self?.presenter?.handleHeroStatsErrorRequest(response: error)
            }
        ).call()
    }
}
