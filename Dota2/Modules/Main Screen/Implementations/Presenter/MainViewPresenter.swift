//
//  MainViewPresenter.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation
import API

class MainViewPresenter: MainViewPresenterProtocol {
    
    var interactor: MainViewInteractorProtocol?
    weak var view: MainViewControllerProtocol?
    var router: MainViewRouterProtocol?
    
    func handleFetchHeroStats(withQuery query: String?) {
        interactor?.fetchHeroStats(withQuery: query)
    }
    
    func handleHeroStatsSuccessRequest(response: [HeroStatsResponse]) {
        view?.showHeroStats(response)
    }
    
    func handleHeroStatsErrorRequest(response: ErrorResponse) {
        view?.showErrorMessage(response.message)
    }
}
