//
//  MainViewRouter.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation

class MainViewRouter: MainViewRouterProtocol {
    class func createMainModule(mainRef: MainViewController) {
        let presenter: MainViewPresenterProtocol = MainViewPresenter()
        
        mainRef.presenter = presenter
        mainRef.presenter?.router = MainViewRouter()
        mainRef.presenter?.view = mainRef
        mainRef.presenter?.interactor = MainViewInteractor()
        mainRef.presenter?.interactor?.presenter = presenter
    }
}
