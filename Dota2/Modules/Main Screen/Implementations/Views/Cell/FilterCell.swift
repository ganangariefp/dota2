//
//  FilterCell.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 07/07/21.
//

import UIKit

class FilterCell: BaseCell {
    
    let filterLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = .boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        addSubview(filterLabel)
    }
    
    func setupConstraint() {
        filterLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        filterLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
