//
//  HeaderCell.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 07/07/21.
//

import UIKit

class HeaderCell: BaseCell {
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        addSubview(headerLabel)
    }
    
    func setupConstraint() {
        headerLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        headerLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 16).isActive = true
    }
}
