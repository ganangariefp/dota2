//
//  HeroCell.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit
import API
import Kingfisher

class HeroCell: BaseCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 10)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let statsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 10)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.text = "Statistics"
        
        return label
    }()
    
    let rolesLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 4
        
        return label
    }()
    
    let attributeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let attackTypeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let baseHealthLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let baseAttackLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let baseManaLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let baseSpeedLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 8)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        return label
    }()
    
    let photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .lightGray
        imageView.clipsToBounds = true

        return imageView
    }()
    
    let favoriteButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Favorite", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14)
        
        return button
    }()

    override func setupViews() {
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        addSubview(nameLabel)
        addSubview(photoImageView)
        addSubview(statsLabel)
        addSubview(rolesLabel)
        addSubview(attributeLabel)
        addSubview(attackTypeLabel)
        addSubview(baseHealthLabel)
        addSubview(baseAttackLabel)
        addSubview(baseManaLabel)
        addSubview(baseSpeedLabel)
        addSubview(favoriteButton)
    }
    
    func setupConstraint() {
        photoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        photoImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        photoImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
        photoImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        photoImageView.layer.cornerRadius = 40
        
        nameLabel.topAnchor.constraint(equalTo: photoImageView.topAnchor).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: photoImageView.trailingAnchor, constant: 12).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        rolesLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4).isActive = true
        rolesLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor).isActive = true
        rolesLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        
        attackTypeLabel.topAnchor.constraint(equalTo: rolesLabel.bottomAnchor, constant: 4).isActive = true
        attackTypeLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor).isActive = true
        attackTypeLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        statsLabel.topAnchor.constraint(equalTo: photoImageView.bottomAnchor, constant: 8).isActive = true
        statsLabel.leadingAnchor.constraint(equalTo: photoImageView.leadingAnchor).isActive = true
        
        baseAttackLabel.topAnchor.constraint(equalTo: statsLabel.bottomAnchor, constant: 8).isActive = true
        baseAttackLabel.leadingAnchor.constraint(equalTo: statsLabel.leadingAnchor).isActive = true
        
        baseHealthLabel.topAnchor.constraint(equalTo: baseAttackLabel.bottomAnchor, constant: 4).isActive = true
        baseHealthLabel.leadingAnchor.constraint(equalTo: statsLabel.leadingAnchor).isActive = true
        
        baseManaLabel.topAnchor.constraint(equalTo: baseHealthLabel.bottomAnchor, constant: 4).isActive = true
        baseManaLabel.leadingAnchor.constraint(equalTo: statsLabel.leadingAnchor).isActive = true
        
        baseSpeedLabel.topAnchor.constraint(equalTo: baseManaLabel.bottomAnchor, constant: 4).isActive = true
        baseSpeedLabel.leadingAnchor.constraint(equalTo: statsLabel.leadingAnchor).isActive = true
        
        favoriteButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        favoriteButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        favoriteButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        favoriteButton.layer.borderWidth = 1
        favoriteButton.layer.cornerRadius = 4
        favoriteButton.layer.borderColor = UIColor.gray.cgColor
    }
    
    func setHero(_ hero: HeroStatsResponse) {
        let url = URL(string: Constant.apiBaseImagePath + hero.img!)
        photoImageView.kf.setImage(with: url)
        nameLabel.text = hero.localized_name
        rolesLabel.text = "".roles(hero.roles!)
        attackTypeLabel.text = "Attack Type = " + (hero.attack_type ?? "")
        baseAttackLabel.text = "Base Attack = \(String(hero.base_attack_min ?? 0)) - \(String(hero.base_attack_max ?? 0))"
        baseHealthLabel.text = "Base Health = \(String(hero.base_health ?? 0))"
        baseManaLabel.text = "Base Mana = \(String(hero.base_mana ?? 0))"
        baseSpeedLabel.text = "Base Speed = \(String(hero.move_speed ?? 0))"
    }
    
    func setHeroFavorite(_ hero: HeroEntity) {
        photoImageView.image = UIImage(data: hero.img!)
        nameLabel.text = hero.name
        attackTypeLabel.text = "Attack Type = " + (hero.attack_type ?? "")
    }
}
