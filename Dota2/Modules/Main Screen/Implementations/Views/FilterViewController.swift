//
//  FilterViewController.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit

protocol FilterDelegate {
    func handleFilterAndSort(_ filter: [String],_ kind: SortType.RawValue,_ order: OrderType.RawValue, _ rawSort: String)
}

class FilterViewController: UIViewController {
    
    var delegate: FilterDelegate!
    
    var sortArray = ["Lowest Health", "Highest Health", "Lowest Mana", "Highest Mana", "Lowest Speed", "Highest Speed", "Lowest Attack", "Highest Attack"]
    var sortTypeArray = [SortType.base_health.rawValue, SortType.base_health.rawValue, SortType.base_mana.rawValue, SortType.base_mana.rawValue, SortType.move_speed.rawValue, SortType.move_speed.rawValue, SortType.base_attack_min.rawValue, SortType.base_attack_min.rawValue]
    var orderTypeArray = [OrderType.min.rawValue, OrderType.max.rawValue, OrderType.min.rawValue, OrderType.max.rawValue, OrderType.min.rawValue, OrderType.max.rawValue, OrderType.min.rawValue, OrderType.max.rawValue]
    var selectedSort: SortType.RawValue? {
        didSet {
        }
    }
    var selectedOrder: OrderType.RawValue? {
        didSet {
        }
    }
    var roles = ["Carry", "Escape", "Nuker", "Initiator", "Durable", "Disabler", "Jungler", "Support", "Pusher"]
    var selectedRoles: [String]? {
        didSet {
        }
    }
    var selectedRawSort: String? {
        didSet {}
    }
    
    let headerPresentView: ModalHeaderView = {
        let view = ModalHeaderView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let sortTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.keyboardDismissMode = .onDrag
        
        return tableView
    }()
    
    let filterCollectionView: UICollectionView = {
        let collectionview = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionview.translatesAutoresizingMaskIntoConstraints = false
        collectionview.backgroundColor = .white
        collectionview.showsVerticalScrollIndicator = false
        
        return collectionview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        registerCollectionView()
    }
    
    func setupViews() {
        view.backgroundColor = .white
        
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        view.addSubview(headerPresentView)
        headerPresentView.backButton.addTarget(self, action: #selector(handleBackButton), for: .touchUpInside)
        headerPresentView.saveButton.addTarget(self, action: #selector(handleSaveButton), for: .touchUpInside)
        
        view.addSubview(filterCollectionView)
        view.addSubview(sortTableView)
    }
    
    func setupConstraint() {
        headerPresentView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerPresentView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        headerPresentView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        headerPresentView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        filterCollectionView.topAnchor.constraint(equalTo: headerPresentView.bottomAnchor, constant: 16).isActive = true
        filterCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        filterCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        filterCollectionView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.18).isActive = true
        
        sortTableView.topAnchor.constraint(equalTo: filterCollectionView.bottomAnchor).isActive = true
        sortTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        sortTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        sortTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

// MARK: Handle Actions Button
extension FilterViewController {
    @objc func handleBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSaveButton() {
        self.dismiss(animated: true, completion: nil)
        delegate.handleFilterAndSort(selectedRoles!, selectedSort!, selectedOrder!, selectedRawSort!)
    }
}

// MARK: Configure collectionview
extension FilterViewController {
    func registerCollectionView() {
        filterCollectionView.delegate = self
        filterCollectionView.dataSource = self
        filterCollectionView.register(FilterCell.self, forCellWithReuseIdentifier: Constant.KEY_FILTER_CELL)
        filterCollectionView.register(HeaderCell.self, forSupplementaryViewOfKind:
                                        UICollectionView.elementKindSectionHeader, withReuseIdentifier: Constant.KEY_HEADER_CELL)
        
        sortTableView.dataSource = self
        sortTableView.delegate = self
        sortTableView.register(SortCell.self, forCellReuseIdentifier: Constant.KEY_SORT_CELL)
    }
}

// MARK: Handle filter collectionview event
extension FilterViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: Constant.KEY_HEADER_CELL, for: indexPath) as! HeaderCell
        header.backgroundColor = .lightGray
        header.headerLabel.text = "Filter"
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10.0, left: 1.0, bottom: 1.0, right: 1.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 28)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return roles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.KEY_FILTER_CELL, for: indexPath) as! FilterCell
        cell.filterLabel.text = roles[indexPath.row]
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 4
        cell.layer.borderColor = UIColor.gray.cgColor
        
        cell.backgroundColor = (selectedRoles?.contains(roles[indexPath.row]))! ? .green : .white
            
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 24)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedRoles!.contains(roles[indexPath.row]) {
            let index = selectedRoles?.firstIndex(of: roles[indexPath.row])
            selectedRoles!.remove(at: index!)
            let cell = filterCollectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = .white
        } else {
            selectedRoles?.append(roles[indexPath.row])
            let cell = filterCollectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = .green
        }
    }
}

// MARK: Handle Tableview delegate and datasource
extension FilterViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Sort"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.KEY_SORT_CELL, for: indexPath) as! SortCell
        cell.sortLabel.text = sortArray[indexPath.row]
        cell.backgroundColor = sortArray[indexPath.row] == selectedRawSort ? .black : .white
        cell.sortLabel.textColor = sortArray[indexPath.row] == selectedRawSort ? .white : .black
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSort = sortTypeArray[indexPath.row]
        selectedOrder = orderTypeArray[indexPath.row]
        selectedRawSort = sortArray[indexPath.row]
    }
}

