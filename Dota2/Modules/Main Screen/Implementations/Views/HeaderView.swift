//
//  HeaderView.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit

class HeaderView: BaseView {
    
    let filterButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Filter", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14)
        button.setImage(UIImage(named: "filter"), for: .normal)
        button.clipsToBounds = true
        button.tintColor = .black
        button.imageView?.contentMode = .scaleAspectFit
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: 10, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 10, left: -60, bottom: 10, right: 0)
        button.showsTouchWhenHighlighted = true
        button.isEnabled = false
        
        return button
    }()
    
    let sortButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Sort", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 14)
        button.setImage(UIImage(named: "down-arrow"), for: .normal)
        button.tintColor = .black
        button.clipsToBounds = true
        button.imageView?.contentMode = .scaleAspectFit
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 1
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.imageEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: 10, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 10, left: -60, bottom: 10, right: 0)
        button.showsTouchWhenHighlighted = true
        button.isEnabled = false
        
        return button
    }()
    
    override func setupViews() {
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        addSubview(filterButton)
        addSubview(sortButton)
    }
    
    func setupConstraint() {
        filterButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        filterButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        filterButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        filterButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        filterButton.layer.cornerRadius = 8
        
        sortButton.centerYAnchor.constraint(equalTo: filterButton.centerYAnchor).isActive = true
        sortButton.leadingAnchor.constraint(equalTo: filterButton.trailingAnchor, constant: 16).isActive = true
        sortButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        sortButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        sortButton.layer.cornerRadius = 8
    }
}
