//
//  ViewController.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 05/07/21.
//

import UIKit
import API
import CoreData

class MainViewController: UIViewController {

    var presenter: MainViewPresenterProtocol?
    var mainView: MainView!
    var heroesStats: [HeroStatsResponse] = []
    var mutationHeroesStats: [HeroStatsResponse] = []
    var selectedRoles: [String] = []
    var selectedSort: SortType.RawValue = ""
    var selectedOrder: OrderType.RawValue = ""
    var selectedRawSort = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupDepedency()
        getHeroStats()
        registerCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.title = "Heroes"
    }
    
    override func loadView() {
        mainView = MainView()
        mainView.headerView.filterButton.addTarget(self, action: #selector(handleFilterButton), for: .touchUpInside)
        mainView.headerView.sortButton.addTarget(self, action: #selector(handleSortButton), for: .touchUpInside)
        view = mainView
        view.backgroundColor = .white
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

}

// MARK: Configure collectionview
extension MainViewController {
    func registerCollectionView() {
        mainView.heroesCollectionView.delegate = self
        mainView.heroesCollectionView.dataSource = self
        mainView.heroesCollectionView.register(HeroCell.self, forCellWithReuseIdentifier: Constant.KEY_HERO_CELL)
    }
}


// MARK: Handle Buttons Actions
extension MainViewController {
    @objc func handleFilterButton() {
        let vc = FilterViewController()
        vc.delegate = self
        vc.selectedRoles = selectedRoles
        vc.selectedOrder = selectedOrder
        vc.selectedSort = selectedSort
        vc.selectedRawSort = selectedRawSort
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func handleSortButton() {
        let vc = SortViewController()
        vc.delegate = self
        vc.selectedRawSort = selectedRawSort
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func handleFavoriteButton(_ sender: UIButton) {
        let hero = heroesStats.filter { $0.id == sender.tag }
        
        CoreDataManager.shared.addFavoriteHero(hero.first!) { (hero) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "update"), object: "update", userInfo: nil)
        } onError: { (Error) in
            print(Error)
        }

    }
}



// MARK: Handle Depedency
extension MainViewController {
    func setupDepedency() {
        MainViewRouter.createMainModule(mainRef: self)
    }
}

// MARK: Handle Setup Services
extension MainViewController {
    func getHeroStats() {
        presenter?.handleFetchHeroStats(withQuery: "suram")
    }
}

// MARK: Handle Output Service
extension MainViewController: MainViewControllerProtocol {

    func showHeroStats(_ heroStats: [HeroStatsResponse]) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.mutationHeroesStats = heroStats
            self.heroesStats = heroStats
            self.mainView.heroesCollectionView.reloadData()
            self.mainView.headerView.filterButton.isEnabled = true
            self.mainView.headerView.sortButton.isEnabled = true
        })
    }
    
    func showErrorMessage(_ message: String) {
        print(message)
    }
}

// MARK: Handle menu collectionview event
extension MainViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mutationHeroesStats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.KEY_HERO_CELL, for: indexPath) as! HeroCell
        cell.layer.cornerRadius = 8
        cell.backgroundColor = .black
        cell.setHero(mutationHeroesStats[indexPath.row])
        cell.favoriteButton.addTarget(self, action: #selector(handleFavoriteButton), for: .touchUpInside)
        cell.favoriteButton.tag = mutationHeroesStats[indexPath.row].id!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/2 - 24, height: 220)
    }
}

// MARK: Handle Sort Delegate
extension MainViewController: SortDelegate {
    func handleSort(_ kind: SortType.RawValue, _ order: OrderType.RawValue, _ rawSort: String) {
        
        selectedOrder = order
        selectedSort = kind
        selectedRawSort = rawSort
        mutationHeroesStats = heroesStats
        
        if !selectedRoles.isEmpty {
            let filterArr = mutationHeroesStats.filter { ($0.roles?.contains(where: { selectedRoles.contains($0) }))! }
            mutationHeroesStats = filterArr
        }
        
        if !selectedRawSort.isEmpty {
            mainView.headerView.sortButton.setTitle(selectedRawSort, for: .normal)
            mainView.headerView.sortButton.setImage(nil, for: .normal)
            mainView.headerView.sortButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        }
        
        mutationHeroesStats = mutationHeroesStats.sortedByKindAndOrder(mutationHeroesStats, kind, order)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.mainView.heroesCollectionView.setContentOffset(.zero, animated: false)
            self.mainView.heroesCollectionView.reloadData()
        })
        
    }
}

extension MainViewController: FilterDelegate {
    
    func handleFilterAndSort(_ filter: [String],_ kind: SortType.RawValue,_ order: OrderType.RawValue,_ rawSort: String) {
        
        mutationHeroesStats = heroesStats
        selectedSort = kind
        selectedOrder = order
        selectedRawSort = rawSort
        
        if !filter.isEmpty {
            selectedRoles = filter
            
            let filterArr = mutationHeroesStats.filter { ($0.roles?.contains(where: { filter.contains($0) }))! }
            mutationHeroesStats = filterArr
        }
        
        if !selectedRawSort.isEmpty {
            mainView.headerView.sortButton.setTitle(selectedRawSort, for: .normal)
            mainView.headerView.sortButton.setImage(nil, for: .normal)
            mainView.headerView.sortButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        }
        
        mutationHeroesStats = mutationHeroesStats.sortedByKindAndOrder(mutationHeroesStats, kind, order)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.mainView.heroesCollectionView.setContentOffset(.zero, animated: false)
            self.mainView.heroesCollectionView.reloadData()
        })
        
    }
}

