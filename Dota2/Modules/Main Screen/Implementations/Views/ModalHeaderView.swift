//
//  ModalHeaderView.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit

class ModalHeaderView: BaseView {
    
    let backButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Back", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        
        return button
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Save", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 14)
        
        return button
    }()
    
    override func setupViews() {
        super.setupViews()
        
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        addSubview(backButton)
        addSubview(saveButton)
    }
    
    func setupConstraint() {
        backButton.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        backButton.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        backButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        backButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
        
        saveButton.topAnchor.constraint(equalTo: topAnchor, constant: 16).isActive = true
        saveButton.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        saveButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        saveButton.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
}
