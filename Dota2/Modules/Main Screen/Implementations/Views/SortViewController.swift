//
//  SortViewController.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import UIKit

protocol SortDelegate {
    func handleSort(_ kind: SortType.RawValue, _ order: OrderType.RawValue,_ rawSort: String)
}

enum SortType: String {
    case base_health
    case base_mana
    case move_speed
    case base_attack_min
}

enum OrderType: String {
    case min
    case max
}

class SortViewController: UIViewController {
    
    var delegate: SortDelegate!
    var sortArray = ["Lowest Health", "Highest Health", "Lowest Mana", "Highest Mana", "Lowest Speed", "Highest Speed", "Lowest Attack", "Highest Attack"]
    var sortTypeArray = [SortType.base_health.rawValue, SortType.base_health.rawValue, SortType.base_mana.rawValue, SortType.base_mana.rawValue, SortType.move_speed.rawValue, SortType.move_speed.rawValue, SortType.base_attack_min.rawValue, SortType.base_attack_min.rawValue]
    var orderTypeArray = [OrderType.min.rawValue, OrderType.max.rawValue, OrderType.min.rawValue, OrderType.max.rawValue, OrderType.min.rawValue, OrderType.max.rawValue, OrderType.min.rawValue, OrderType.max.rawValue]
    var selectedSort: SortType.RawValue?
    var selectedOrder: OrderType.RawValue?
    var selectedRawSort: String? {
        didSet {}
    }
    
    let headerPresentView: ModalHeaderView = {
        let view = ModalHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    lazy var sortTableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        tableView.register(SortCell.self, forCellReuseIdentifier: Constant.KEY_SORT_CELL)
        
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func setupViews() {
        view.backgroundColor = .white
        
        setupInterfaceComponent()
        setupConstraint()
    }
    
    func setupInterfaceComponent() {
        view.addSubview(headerPresentView)
        headerPresentView.backButton.addTarget(self, action: #selector(handleBackButton), for: .touchUpInside)
        headerPresentView.saveButton.addTarget(self, action: #selector(handleSaveButton), for: .touchUpInside)
        
        view.addSubview(sortTableView)
    }
    
    func setupConstraint() {
        headerPresentView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerPresentView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        headerPresentView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        headerPresentView.heightAnchor.constraint(equalToConstant: 48).isActive = true
        
        sortTableView.topAnchor.constraint(equalTo: headerPresentView.bottomAnchor).isActive = true
        sortTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        sortTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        sortTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

// MARK: Handle Actions Button
extension SortViewController {
    @objc func handleBackButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func handleSaveButton() {
        
        if selectedSort != nil && selectedOrder != nil {
            self.delegate.handleSort(selectedSort ?? "", selectedOrder ?? "", selectedRawSort ?? "")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Handle Tableview delegate and datasource
extension SortViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.KEY_SORT_CELL, for: indexPath) as! SortCell
        cell.sortLabel.text = sortArray[indexPath.row]
        cell.backgroundColor = sortArray[indexPath.row] == selectedRawSort ? .black : .white
        cell.sortLabel.textColor = sortArray[indexPath.row] == selectedRawSort ? .white : .black
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSort = sortTypeArray[indexPath.row]
        selectedOrder = orderTypeArray[indexPath.row]
        selectedRawSort = sortArray[indexPath.row]
    }
}
