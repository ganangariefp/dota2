//
//  MainViewControllerProtocol.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation
import API

protocol MainViewControllerProtocol: class {
    func showHeroStats(_ heroStats: [HeroStatsResponse])
    func showErrorMessage(_ message: String)
}
