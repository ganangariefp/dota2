//
//  MainViewInteractorProtocol.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation

protocol MainViewInteractorProtocol: class {
    var presenter: MainViewPresenterProtocol? { get set }
    
    func fetchHeroStats(withQuery query: String?)
}
