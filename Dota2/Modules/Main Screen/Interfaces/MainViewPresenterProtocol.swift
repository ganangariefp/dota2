//
//  MainViewPresenterProtocol.swift
//  Dota2
//
//  Created by Ganang Arief Pratama on 06/07/21.
//

import Foundation
import API

protocol MainViewPresenterProtocol: class {
    var interactor: MainViewInteractorProtocol? { get set }
    var view: MainViewControllerProtocol? { get set }
    var router: MainViewRouterProtocol? { get set }
    
    func handleFetchHeroStats(withQuery query: String?)
    func handleHeroStatsSuccessRequest(response: [HeroStatsResponse])
    func handleHeroStatsErrorRequest(response: ErrorResponse)
}
