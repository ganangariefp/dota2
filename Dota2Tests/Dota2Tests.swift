//
//  Dota2Tests.swift
//  Dota2Tests
//
//  Created by Ganang Arief Pratama on 05/07/21.
//

import XCTest
import API
import Alamofire

@testable import Dota2

class Dota2Tests: XCTestCase {
    var request: DataRequest?
    
    // Test Case 1 : Test fetching heroes success,
    func testFetchHeroesSuccess() {
        
        let heroesExpectation = expectation(description: "heroesStats")
        var heroesResponse: [HeroStatsResponse]?
        
        request = HeroServices.getHeroStats(
            query: nil,
            page: nil,
            onSuccess: { response in
                heroesResponse = response
                heroesExpectation.fulfill()
            },
            onFailure: { error in
            }
        ).call()
        
        waitForExpectations(timeout: 30) { (error) in
            XCTAssertNotNil(heroesResponse)
        }
    }
    
    // Test Case 2 : Test fetching heroes error,
    func testFetchHeroesError() {
        let errorExpectation = expectation(description: "Error")
        var errorResponse: String?
        
        request = HeroServices.getHeroStats(
            query: "--------------",
            page: 100000000,
            onSuccess: { response in
            },
            onFailure: { error in
                errorResponse = error.message
                errorExpectation.fulfill()
            }
        ).call()
        
        // MARK: SHOULD RETURN ERROR, BUT NO BECAUSE THE API
        waitForExpectations(timeout: 30) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
    
    // Test Case 3 : Test Base URL equal
    func testBaseUrl() {
        XCTAssertEqual(Constant.apiBasePath, "https://api.opendota.com/api")
    }
    
}
